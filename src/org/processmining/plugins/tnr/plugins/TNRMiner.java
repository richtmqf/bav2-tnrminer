/*
 * Copyright (c) 2017 Markus Richter.
 * This library is distributed under the terms of the Lesser General Public License, version 3.
 */

package org.processmining.plugins.tnr.plugins;


import org.deckfour.xes.classification.XEventClassifier;
import org.deckfour.xes.info.impl.XLogInfoImpl;
import org.deckfour.xes.model.XLog;
import org.processmining.contexts.uitopia.annotations.UITopiaVariant;
import org.processmining.framework.plugin.PluginContext;
import org.processmining.framework.plugin.annotations.Plugin;
import org.processmining.plugins.tnr.common.*;

import java.util.List;

/**
 * This is a ProM-Plugin for generating a TNR(Temporal Network Representation)-Graph from an event log.
 * The basic principles for this can be read in [1].
 *
 * @author Markus Richter
 * @see TNRViewer
 * <p>
 * [1] Arik Senderovich, Matthias Weidlich, and Avigdor Gal. Temporal network representation of event logs for improved
 * performance modelling in business processes.
 */
public class TNRMiner {
	@Plugin(
			name = "TNRMiner v0.0.2",
			parameterLabels = {"input log"},
			returnLabels = {"TNR-Graph"},
			returnTypes = {TNRGraph.class},
			help = "Generates a TNR(Temporal Network Representation)-Graph from an event log. See [1].\n\n" +
					"[1] Arik Senderovich, Matthias Weidlich, and Avigdor Gal. Temporal network representation " +
					"of event logs for improved performance modelling in business processes."
	)
	@UITopiaVariant(
			affiliation = "HU Berlin",
			author = "Markus Richter",
			email = "marku.online@googlemail.com"
	)
	public static TNRGraph mineTNR(PluginContext context, XLog log) {
		XEventClassifier fixedEC = getEventClassifier(log);

		TNRGraph tnrg = new TNRGraph();
		IntervalLog iLog = new IntervalLog(log,fixedEC);
		for(IntervalTrace trace : iLog) {
			addTrace(tnrg, trace);
		}
		return tnrg;
	}

	/**
	 * This function processes a log and adds all the containing activty relations to the given graph.
	 *
	 * @param tg  The graph to which all interval relations of the trace are added.
	 * @param trace  The trace which will be analyzed.
	 */
	private static void addTrace(TNRGraph tg, final IntervalTrace trace) {
		//add all activities from events to the graph if they don't exist yet
		// and also add their execution lengths to the statistics of the activity
		for(IntervalEvent event : trace) {
			tg.getOrAddNode(event.getLbl())
					.getEvClass().appendDuration(event.getCompleteTimestamp().getTime()-event.getStartTimestamp().getTime());
		}

		//compare all events of the trace with each other and add an edge(-count) for each relation.
		for(IntervalEvent event1 : trace) {
			for(IntervalEvent event2 : trace) {
				if(event1 == event2) continue;

				AllenRelation rel = AllenRelation.determine(
						event1.getStartTimestamp().getTime(),
						event1.getCompleteTimestamp().getTime(),
						event2.getStartTimestamp().getTime(),
						event2.getCompleteTimestamp().getTime());
				//limit to these relation types to prevent drawing the same edge 2 times.
				switch(rel) {
					case PRECEDES:
					case MEETS:
					case OVERLAPS:
					case IS_FINISHED_BY:
					case CONTAINS:
					case STARTS:
					case EQUALS:
						tg.addOrIncrementEdge(event1.getLbl(), event2.getLbl(), rel);
				}
			}
		}
	}

	/**
	 * Determines an EventClassifier based on the given log, which fits this package the most
	 * 		(preferred: XLogInfoImpl.NAME_CLASSIFIER)
	 * @param log
	 * @return the chosen XEventClassifier
	 */
	static XEventClassifier getEventClassifier(XLog log){
		List<XEventClassifier> xeclist = log.getClassifiers();
		for(XEventClassifier xec : xeclist) {
			if(xec.name().equals("Event Name"))
				return xec;
		}
		if(xeclist.size()>0)
			return xeclist.get(0);
		else
			return XLogInfoImpl.NAME_CLASSIFIER;
	}
}


