/*
 * Copyright (c) 2017 Markus Richter.
 * This library is distributed under the terms of the Lesser General Public License, version 3.
 */

package org.processmining.plugins.tnr.plugins;

import org.deckfour.xes.model.XLog;
import org.processmining.contexts.uitopia.annotations.UITopiaVariant;
import org.processmining.framework.plugin.PluginContext;
import org.processmining.framework.plugin.annotations.Plugin;
import org.processmining.plugins.tnr.common.*;

import java.util.ArrayList;
import java.util.List;

public class DependencyDelayFinder {
	@Plugin(
			name = "Discover Delay-Causing Dependencies.",
			parameterLabels = {"Process Log", "TNR"},
			returnLabels = {"Dependencies", "Statistics"},
			returnTypes = {DependencyMapping.class, String.class},
			help = "Discovers Dependencies by checking their interval relations on a case-by-case basis."
	)
	@UITopiaVariant(
			affiliation = "HU Berlin",
			author = "Markus Richter",
			email = "marku.online@googlemail.com"
	)
	public static Object[] discoverDependencies(PluginContext context, XLog xlog, TNRGraph tnr) {
		//preparations: transform log into IntervalLog
		// (merge start and end events and select an activity label with the classifier)
		IntervalLog log = new IntervalLog(xlog,TNRMiner.getEventClassifier(xlog));
		int stats[] = new int[]{/* # original keys*/0,/* # original values*/0,/* # reduced keys*/0,/* # reduced values*/0,
		/* # outer cycles*/ 0};


		//first step: get all Candidates for dependency relations (must have at least meets and only precedes is allowed, but not needed)
		DependencyMapping dependencyCandidates = new DependencyMapping();
		for(TNREdge edge : tnr.getEdges()) {
			if(edge.getF(AllenRelation.MEETS) > 0
					&& edge.getF(AllenRelation.OVERLAPS) == 0
					&& edge.getF(AllenRelation.IS_FINISHED_BY) == 0
					&& edge.getF(AllenRelation.CONTAINS) == 0
					&& edge.getF(AllenRelation.STARTS) == 0
					&& edge.getF(AllenRelation.EQUALS) == 0
					//no reflexive edges!
					&& !edge.getTarget().equals(edge.getSource()))
			{
				dependencyCandidates.put(edge.getTarget().getEvClass(),edge.getSource().getEvClass());
			}
		}
		//statistics for debugging
		stats[0] = dependencyCandidates.keySet().size();
		stats[1] = dependencyCandidates.values().size();

		//second step: reduce candidates till they are minimal.
		boolean candidatesChanged;
		do {
			++stats[4];
			candidatesChanged = false;
			for(Object o : dependencyCandidates.keySet()) {
				EventClass activity2 = (EventClass) o;
				IntervalEvent ev1, ev2;
				for(IntervalTrace trace : log) {
					if((ev2 = find(trace, activity2)) != null) {
						//if there is an event with an activity from our dependency Candidates and their relation is
						// MEETS, all other precedes can be explained in this trace, so no candidate is deleted.
						//
						//If no such event is found(all candidates represented in the trace have precedes), all
						// these events can't be candidates.

						List<EventClass> deleteCandidates = new ArrayList<>();
						for(EventClass activity1 : dependencyCandidates.get(activity2)) {
							if((ev1 = find(trace, activity1)) != null){
									AllenRelation rel = AllenRelation.determine(
										ev1.getStartTimestamp().getTime(),
										ev1.getCompleteTimestamp().getTime(),
										ev2.getStartTimestamp().getTime(),
										ev2.getCompleteTimestamp().getTime());

								if(rel == AllenRelation.MEETS) {
									deleteCandidates.clear();
									break;
								}else if(rel ==AllenRelation.PRECEDES){
									deleteCandidates.add(activity1);
								}
							}
						}
						//delete all candidates appearing in the trace, because only precedes-relations were found.
						if(deleteCandidates.size()!=0) {
							dependencyCandidates.get(activity2).removeAll(deleteCandidates);
							candidatesChanged = true;
						}
					}
				}
			}
			//remove all empty candidates for optimization.
			List<EventClass> emptyCandidates = new ArrayList<>();
			for(EventClass activity : dependencyCandidates.keySet()) {
				if(dependencyCandidates.get(activity).size()<=0)/*..*/
					emptyCandidates.add(activity);
			}
			emptyCandidates.forEach(dependencyCandidates::remove);

		}while(candidatesChanged);
		//statistics
		stats[2] = dependencyCandidates.keySet().size();
		stats[3] = dependencyCandidates.values().size();
		String statistics=String.format("after 1: k/v: %d/%d" +
				"\tafter 2: k/v: %d/%d" +
				"\touter cycles: %d", stats[0], stats[1], stats[2], stats[3], stats[4]);

		System.err.println(""+dependencyCandidates);

		return new Object[]{dependencyCandidates,statistics};
	}


	private static IntervalEvent find(IntervalTrace trace, EventClass activity){
		for(IntervalEvent event : trace) {
			//Assumption: there are no same activities/labels in one trace
			if(activity.getId().equals(event.getLbl()))
				return event;
		}
		return null;
	}
}
