/*
 * Copyright (c) 2017 Markus Richter.
 * This library is distributed under the terms of the Lesser General Public License, version 3.
 */

package org.processmining.plugins.tnr.plugins;

import org.deckfour.xes.model.XLog;
import org.processmining.contexts.uitopia.annotations.UITopiaVariant;
import org.processmining.framework.plugin.PluginContext;
import org.processmining.framework.plugin.annotations.Plugin;
import org.processmining.plugins.tnr.common.EnrichedTree;

public class CombinedWorkflow {
	@Plugin(
			name = "BA-Workflow with dependencies",
			parameterLabels = {"Process Log"},
			returnLabels = {"Statistics"},
			returnTypes = {String.class},
			help = "Executes the whole Workflow for a process log and outputs the Results."
	)
	@UITopiaVariant(
			affiliation = "HU Berlin",
			author = "Markus Richter",
			email = "marku.online@googlemail.com"
	)
	public static String baTree1(PluginContext context, XLog log) {
		return baCommon(context, log,true);
	}

	@Plugin(
			name = "BA-Workflow without dependencies",
			parameterLabels = {"Process Log"},
			returnLabels = {"Statistics"},
			returnTypes = {String.class},
			help = "Executes the whole Workflow for a process log and outputs the Results."
	)
	@UITopiaVariant(
			affiliation = "HU Berlin",
			author = "Markus Richter",
			email = "marku.online@googlemail.com"
	)
	public static String baTree2(PluginContext context, XLog log) {
		return baCommon(context, log,false);
	}

	private static String baCommon(PluginContext context, XLog inputLog, boolean withDeps){
		XLog log=FilterLoops.filterLoops(context,inputLog);

		EnrichedTree t = EnrichProcessTree.treeCommon(context,log,withDeps);
		String results = PerformanceFitness.measureFitness(context,t,log);
		String out = withDeps?"with ConditionalDelay: ":"pure: ";
		return out + results;
	}
}
