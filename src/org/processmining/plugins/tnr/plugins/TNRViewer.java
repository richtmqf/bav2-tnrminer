/*
 * Copyright (c) 2017 Markus Richter.
 * This library is distributed under the terms of the Lesser General Public License, version 3.
 */

package org.processmining.plugins.tnr.plugins;

import org.processmining.contexts.uitopia.annotations.UITopiaVariant;
import org.processmining.contexts.uitopia.annotations.Visualizer;
import org.processmining.framework.plugin.PluginContext;
import org.processmining.framework.plugin.annotations.Plugin;
import org.processmining.models.graphbased.AttributeMap;
import org.processmining.models.graphbased.ViewSpecificAttributeMap;
import org.processmining.models.graphbased.directed.DirectedGraph;
import org.processmining.models.graphbased.directed.DirectedGraphEdge;
import org.processmining.models.graphbased.directed.DirectedGraphNode;
import org.processmining.models.jgraph.ProMJGraphVisualizer;
import org.processmining.plugins.tnr.common.AllenRelation;
import org.processmining.plugins.tnr.common.TNREdge;
import org.processmining.plugins.tnr.common.TNRGraph;
import org.processmining.plugins.tnr.common.TNRNode;

import javax.swing.*;
import java.awt.*;
import java.util.Collection;
import java.util.Set;
import java.util.function.Predicate;
import java.util.stream.Collectors;

/**
 * A small plugin for visualizing a TNR Graph.
 *
 * @author Markus Richter
 * @see TNRMiner
 */
public class TNRViewer {
	/**
	 * @param context The current PluginContext.
	 * @param tg      The TNRGraph, which was selected in the interface or automatically selected after the application of
	 *                a plugin.
	 * @return a automatically generated graph viewer in a JComponent.
	 */
	@Plugin(
			name = "view TNRGraph",
			parameterLabels = {"TNR Graph"},
			returnLabels = {"Visualized TNR-Graph"},
			returnTypes = {JComponent.class},
			help = "Shows the generated TNR Graph."
	)
	@UITopiaVariant(
			affiliation = "HU Berlin",
			author = "Markus Richter",
			email = "marku.online@googlemail.com"
	)
	@Visualizer
	public static JComponent visualize(PluginContext context, TNRGraph tg) {

		//all filtering needs should be put into the FilteredTNRGraph class.
//		FilteredTNRGraph g = new FilteredTNRGraph(tg, x -> x.getRelations().size() > 1 || !x.getRelations().contains(AllenRelation.PRECEDES));
		FilteredTNRGraph g = new FilteredTNRGraph(tg, x -> true);


		//set some viewing preferences
		ViewSpecificAttributeMap map = new ViewSpecificAttributeMap();
		//-show all edge labels
		for(TNREdge e : g.getEdges()) {
			map.putViewSpecific(e, AttributeMap.LABEL, e.getLabel());
			map.putViewSpecific(e, AttributeMap.TOOLTIP, "" + e.getSource().getLabel() + " -> " + e.getTarget().getLabel());
			map.putViewSpecific(e, AttributeMap.SHOWLABEL, true);
			map.putViewSpecific(e, AttributeMap.LABELALONGEDGE, true);
			// e.getF could be left out but serves as a good filter for the relatively expensive getRelations Function.
			if(e.getF(AllenRelation.PRECEDES) == 0 && e.getRelations().size() == 0){
				map.putViewSpecific(e, AttributeMap.EDGECOLOR, Color.RED);
			}
		}
		//-put some arrows on the target size of the edge.
//		map.putViewSpecific(g, AttributeMap.EDGEEND, AttributeMap.ArrowType.ARROWTYPE_DOUBLELINE);
//		map.putViewSpecific(g, AttributeMap.EDGESTART, AttributeMap.ArrowType.ARROWTYPE_DIAMOND);
//		map.putViewSpecific(g, AttributeMap.EDGEMIDDLE, AttributeMap.ArrowType.ARROWTYPE_CIRCLE);
//		map.putViewSpecific(g, AttributeMap.EDGEENDFILLED, true);
//		map.putViewSpecific(g, AttributeMap.EDGEMIDDLEFILLED, true);
//		map.putViewSpecific(g, AttributeMap.EDGESTARTFILLED, true);

		//draw the graph and return the canvas.
		return ProMJGraphVisualizer.instance().visualizeGraph(context, g, map);
	}
}

/**
 * This is just an overlay class to filter edges according to a given lambda function. All functions are implemented in {@link TNRGraph}.
 *
 * @see TNRGraph
 */
class FilteredTNRGraph implements DirectedGraph<TNRNode, TNREdge> {
	//private int minimumCount = 2;
	private Predicate<TNREdge> filter;
	private TNRGraph g;

	FilteredTNRGraph(TNRGraph tg, Predicate<TNREdge> filter) {
		this.g = tg;
		this.filter = filter;
	}

	@Override
	public Set<TNRNode> getNodes() {
		return g.getNodes();
	}

	@Override
	public Set<TNREdge> getEdges() {
		return g.getEdges().stream().filter(filter).collect(Collectors.toSet());
	}

	@Override
	public Collection<TNREdge> getInEdges(DirectedGraphNode node) {
		return g.getInEdges(node).stream().filter(filter).collect(Collectors.toSet());
	}

	@Override
	public Collection<TNREdge> getOutEdges(DirectedGraphNode node) {
		return g.getOutEdges(node).stream().filter(filter).collect(Collectors.toSet());
	}

	@Override
	public void removeEdge(DirectedGraphEdge directedGraphEdge) {
		g.removeEdge(directedGraphEdge);
	}

	@Override
	public void removeNode(DirectedGraphNode directedGraphNode) {
		g.removeNode(directedGraphNode);
	}

	@Override
	public String getLabel() {
		return g.getLabel();
	}

	@Override
	public DirectedGraph<?, ?> getGraph() {
		return this;
	}

	@Override
	public int compareTo(DirectedGraph<TNRNode, TNREdge> o) {
		return g.compareTo(o);
	}

	@Override
	public AttributeMap getAttributeMap() {
		return g.getAttributeMap();
	}
}
