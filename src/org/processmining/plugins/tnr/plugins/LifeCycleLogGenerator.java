/*
 * Copyright (c) 2017 Markus Richter.
 * This library is distributed under the terms of the Lesser General Public License, version 3.
 */

package org.processmining.plugins.tnr.plugins;

import org.deckfour.xes.extension.std.XConceptExtension;
import org.deckfour.xes.extension.std.XLifecycleExtension;
import org.deckfour.xes.extension.std.XTimeExtension;
import org.deckfour.xes.info.impl.XLogInfoImpl;
import org.deckfour.xes.model.XAttributeMap;
import org.deckfour.xes.model.XEvent;
import org.deckfour.xes.model.XLog;
import org.deckfour.xes.model.XTrace;
import org.deckfour.xes.model.impl.*;
import org.processmining.contexts.uitopia.annotations.UITopiaVariant;
import org.processmining.framework.plugin.PluginContext;
import org.processmining.framework.plugin.annotations.Plugin;

import java.time.Instant;
import java.util.Comparator;
import java.util.Date;
import java.util.Random;
import java.util.stream.IntStream;



public class LifeCycleLogGenerator {
	private static Random r = new Random();
	private static final int TRACE_COUNT=1000;
	private static final int DELAY_FACTOR=1;
	private static final long DURATION_MEAN=15L*60L*1000L;//15 minutes

	@Plugin(
			name = "Generate a simple event log with lifecycle and time information.",
			parameterLabels = {},
			returnLabels = {"generated Log"},
			returnTypes = {XLog.class},
			help = "Generate a simple transactional event log with time-annotated intervals and concurrency."
	)
	@UITopiaVariant(
			affiliation = "HU Berlin",
			author = "Markus Richter",
			email = "marku.online@googlemail.com"
	)
	public static XLog dependencyTree1(PluginContext context) {
		return dependencyTree(context,false);
	}

	@Plugin(
			name = "Generate a simple event log with lifecycle and time information(long-D-variant).",
			parameterLabels = {},
			returnLabels = {"generated Log with long D-Activity"},
			returnTypes = {XLog.class},
			help = "Generate a simple transactional event log with time-annotated intervals and concurrency. Activity D gets a doubled time."
	)
	@UITopiaVariant(
			affiliation = "HU Berlin",
			author = "Markus Richter",
			email = "marku.online@googlemail.com"
	)
	public static XLog dependencyTree2(PluginContext context) {
		return dependencyTree(context,true);
	}


	private static XLog dependencyTree(PluginContext context,boolean largeD) {
		XLogImpl log = new XLogImpl(new XAttributeMapImpl());
		//generate TRACE_COUNT traces and add them to the log.
		IntStream.range(0, TRACE_COUNT).mapToObj(i -> generateTrace("" + i,largeD)).forEach(log::add);
		log.getExtensions().add(XConceptExtension.instance());
		log.getExtensions().add(XTimeExtension.instance());
		log.getExtensions().add(XLifecycleExtension.instance());
		log.getClassifiers().add(XLogInfoImpl.NAME_CLASSIFIER);
		return log;
	}

	private static long generateDuration(long mean, int deviationFactor){
		//stddev is .1 of DURATION_MEAN
		double stddev = DURATION_MEAN*.1*deviationFactor;
		long val = (long) (r.nextGaussian()*stddev + mean);
		if(val>=0)
			return val;
		//no negative values, if one is found try again!
		return generateDuration(mean,deviationFactor);
	}

	private static XTrace generateTrace(String id,boolean largeD) {

		//Choose a start time somewhere between now and a year ago. (Basically irrelevant)
		long startTime =  Date.from(Instant.now()).getTime()-(long)(r.nextDouble()*1000*3600*24*365);

		long a,b,c,d;//duration of events
		//b,c,d independently:
		b=generateDuration(DURATION_MEAN,1);
		c=generateDuration(DURATION_MEAN,1);
		long d_duration=DURATION_MEAN*(largeD?2:1);
		d=generateDuration(d_duration,1);
		//a dependent on the length of b
		a=generateDuration(b,DELAY_FACTOR);


		//create a new trace
		XAttributeMap traceAttr = new XAttributeMapImpl();
		//set concept:id
		traceAttr.put(XConceptExtension.KEY_NAME, new XAttributeLiteralImpl(XConceptExtension.KEY_NAME,id));

		XTrace trace=new XTraceImpl(traceAttr);

		//now, add all events:
		generateAndAddEvent(trace,startTime,a,"A",id);
		generateAndAddEvent(trace,startTime,b,"B",id);
		generateAndAddEvent(trace,startTime+Long.max(a,b),c,"C",id);
		generateAndAddEvent(trace,startTime+b,d,"D",id);


		trace.sort(Comparator.comparingLong(o -> XTimeExtension.instance().extractTimestamp(o).getTime()));
		return trace;
	}

	private static void generateAndAddEvent(XTrace trace,long starttime, long duration, String label, String traceid){
		//create and add start event
		XEvent e1=new XEventImpl();
		XConceptExtension.instance()	.assignInstance(e1,traceid+label);
		XConceptExtension.instance()	.assignName(e1,label);
		XTimeExtension.instance()		.assignTimestamp(e1,starttime);
		XLifecycleExtension.instance()	.assignStandardTransition(e1, XLifecycleExtension.StandardModel.START);
		trace.add(e1);

		//create and add completion event
		XEvent e2=new XEventImpl();
		XConceptExtension	.instance().assignInstance(e2,traceid+label);
		XConceptExtension	.instance().assignName(e2,label);
		XTimeExtension		.instance().assignTimestamp(e2,starttime+duration);
		XLifecycleExtension	.instance().assignStandardTransition(e2, XLifecycleExtension.StandardModel.COMPLETE);
		trace.add(e2);
	}
}
