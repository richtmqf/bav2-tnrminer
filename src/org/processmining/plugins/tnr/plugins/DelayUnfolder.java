/*
 * Copyright (c) 2017 Markus Richter.
 * This library is distributed under the terms of the Lesser General Public License, version 3.
 */

package org.processmining.plugins.tnr.plugins;

import org.processmining.contexts.uitopia.annotations.UITopiaVariant;
import org.processmining.framework.plugin.PluginContext;
import org.processmining.framework.plugin.annotations.Plugin;
import org.processmining.plugins.tnr.common.AllenRelation;
import org.processmining.plugins.tnr.common.TNREdge;
import org.processmining.plugins.tnr.common.TNRGraph;
import org.processmining.plugins.tnr.common.TNRNode;

import java.util.HashMap;
import java.util.Map;

public class DelayUnfolder {
	@Plugin(
			name = "Delay Unfolding of TNRs",
			parameterLabels = {"input TNR"},
			returnLabels = {"TNR with DelayUnfolding"},
			returnTypes = {TNRGraph.class},
			help = "Does a delay unfolding on the given TNR"
	)
	@UITopiaVariant(
			affiliation = "HU Berlin",
			author = "Markus Richter",
			email = "marku.online@googlemail.com"
	)
	public static TNRGraph unfoldDelays(PluginContext context, TNRGraph oldTNR) {
		TNRGraph newTNR = oldTNR.clone();

		for(TNREdge oldEdge : oldTNR.getEdges()) {
			String xLbl = oldEdge.getSource().getLabel();
			String yLbl = oldEdge.getTarget().getLabel();

			//label exists, so it isn't null.
			TNREdge newEdge = newTNR.getEdgeOrNull(xLbl, yLbl);
			if(shouldUnfold(oldTNR, oldEdge)) {
				newEdge.setFrequency(AllenRelation.PRECEDES, 0);
				int f = oldEdge.getF(AllenRelation.PRECEDES);
				String zLbl = "δ(" + xLbl + "," + yLbl + ")";
				System.out.println(zLbl + " " + xLbl);
				newTNR.addOrIncrementEdge(xLbl, zLbl, AllenRelation.MEETS, f);
				newTNR.addOrIncrementEdge(zLbl, yLbl, AllenRelation.MEETS, f);
			}
		}
		return newTNR;
	}

	private static boolean shouldUnfold(TNRGraph tnr, TNREdge abEdge) {
		if(abEdge.getF(AllenRelation.PRECEDES)>0) {
			//given the tnrEdge x -> y.
			//if there is a precedes relation from x to y, check if there are no nodes between them:
			//build a set containing all nodes z where x -> z -> y is true.
			//Then, check the relations of x -> z and z -> y for temporal evidence of an unexplained delay.


			//iterate over all incoming edges from y and add the edge and the source node to a map
			Map<TNRNode, TNREdge> zyEdges = new HashMap<>();
			for(TNREdge zyEdge : tnr.getInEdges(abEdge.getTarget())) {
				zyEdges.put(zyEdge.getSource(), zyEdge);
			}
			//iterate over all outgoing edges from x and compare the resulting edges with the existing map.
			for(TNREdge xzEdge : tnr.getOutEdges(abEdge.getSource())) {
				if(zyEdges.containsKey(xzEdge.getTarget())) {
					TNREdge zyEdge = zyEdges.get(xzEdge.getTarget());
					//check x -> z and z -> y: if an explanation for the delay is found, quit with false.
					if((xzEdge.getF(AllenRelation.PRECEDES)>0 || xzEdge.getF(AllenRelation.MEETS)>0) &&
							(zyEdge.getF(AllenRelation.PRECEDES)>0 || zyEdge.getF(AllenRelation.MEETS)>0))
						//delay can be explained with an event z that is after x and before y.
						return false;

					if((xzEdge.getF(AllenRelation.OVERLAPS)>0
							|| xzEdge.getF(AllenRelation.IS_FINISHED_BY)>0
							|| xzEdge.getF(AllenRelation.CONTAINS)>0
							|| xzEdge.getF(AllenRelation.STARTS)>0
							|| xzEdge.getF(AllenRelation.EQUALS)>0))
						//delay can be explained with an event z that starts before or with completion of x.
						return false;
				}
			}
			return true;
		} else {
			return false;
		}
	}
}
