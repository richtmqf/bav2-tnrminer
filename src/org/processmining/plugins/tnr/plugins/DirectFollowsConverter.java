/*
 * Copyright (c) 2017 Markus Richter.
 * This library is distributed under the terms of the Lesser General Public License, version 3.
 */

package org.processmining.plugins.tnr.plugins;

import org.processmining.contexts.uitopia.annotations.UITopiaVariant;
import org.processmining.framework.plugin.PluginContext;
import org.processmining.framework.plugin.annotations.Plugin;
import org.processmining.plugins.InductiveMiner.dfgOnly.Dfg;
import org.processmining.plugins.InductiveMiner.dfgOnly.DfgImpl;
import org.processmining.plugins.tnr.common.AllenRelation;
import org.processmining.plugins.tnr.common.TNREdge;
import org.processmining.plugins.tnr.common.TNRGraph;
import org.processmining.plugins.tnr.common.TNRNode;

import java.util.HashMap;
import java.util.HashSet;
import java.util.Map;
import java.util.Set;

/**
 * This is a ProM-Plugin for converting a TNR(Temporal Network Representation)-Graph to a direct-follows-Graph.
 * The basic principles for this can be read in [1].
 *
 * @author Markus Richter
 * @see TNRMiner
 * <p>
 * [1] Arik Senderovich, Matthias Weidlich, and Avigdor Gal. Temporal network representation of event logs for improved
 * performance modelling in business processes.
 */
public class DirectFollowsConverter {
//THIS WAS A WORK-IN-PROGRESS, TAKE WITH A GRAIN OF SALT!

	@Plugin(
			name = "Convert TNR to DirectlyFollows-Graph (deprecated)",
			parameterLabels = {"input TNR"},
			returnLabels = {"Directly-Follows-Graph", "Statistics"},
			returnTypes = {Dfg.class, String.class},
			help = "Converts the Graph by filtering and merging existing AllenRelation evidences. \n" +
					"This Plug-In can not work for theoretical reasons."
	)
	@UITopiaVariant(
			affiliation = "HU Berlin",
			author = "Markus Richter",
			email = "marku.online@googlemail.com"
	)
	public static Object[] convert2Dfg(PluginContext context, TNRGraph oldTNR) {
		Dfg out = new DfgImpl();
		int del_count = 0;
		for(TNRNode tnrNode : oldTNR.getNodes()) {
			System.out.println(tnrNode);
			out.addActivity(tnrNode.getEvClass());
		}
		Map<String,Set<String>> deletedEdges = new HashMap<>();
		for(TNREdge edge : oldTNR.getEdges()) {
			if(edge.getRelations().size()==0)continue;
			long fParallel = edge.getF(AllenRelation.IS_FINISHED_BY)
					+ edge.getF(AllenRelation.STARTS)
					+ edge.getF(AllenRelation.OVERLAPS)
					+ edge.getF(AllenRelation.CONTAINS)
					+ edge.getF(AllenRelation.EQUALS);
			if(fParallel>0){
				out.addParallelEdge(edge.getSource().getEvClass(),edge.getTarget().getEvClass(),fParallel);
			}

			long fFollows = edge.getF(AllenRelation.PRECEDES)
					+ edge.getF(AllenRelation.MEETS)
					+ edge.getF(AllenRelation.OVERLAPS)
					+ edge.getF(AllenRelation.CONTAINS);

			//+IS_FINISHED_BY -CONTAINS?
			//TRANSITIVE REDUCTION? doesn't work!!!
			if(fFollows==0){
				deletedEdges.putIfAbsent(edge.getSource().getLabel(),new HashSet<>());
				deletedEdges .get(edge.getSource().getLabel()) .add(edge.getTarget().getLabel());
				del_count++;
			} else {
				boolean ab_deleted=false;
				for(TNREdge a_ab : oldTNR.getOutEdges(edge.getSource())) {
					//if the edge a->ab was already deleted, don't consider it for a transitive shell.
					TNRNode a = edge.getSource(),b = edge.getTarget(),ab = a_ab.getTarget();
					if(deletedEdges.containsKey(a.getLabel()) &&
							deletedEdges.get(a.getLabel()).contains(ab.getLabel()))
						continue;
					TNREdge ab_b = oldTNR.getEdgeOrNull(ab.getLabel(), b.getLabel());
					if(ab_b!=null){
						//if the edge a->ab was already deleted, don't consider it for a transitive shell.
						if(deletedEdges.containsKey(ab.getLabel()) &&
								deletedEdges.get(ab.getLabel()).contains(b.getLabel()))
							continue;
						//check if the found edges a_ab and ab_b have a cardinality greater than 0 for
						// the precedes-relations.
						if(ab_b.getF(AllenRelation.PRECEDES)>0
								|| ab_b.getF(AllenRelation.MEETS)>0
								|| ab_b.getF(AllenRelation.OVERLAPS)>0
								|| ab_b.getF(AllenRelation.CONTAINS)>0){
							if(a_ab.getF(AllenRelation.PRECEDES)>0
									|| a_ab.getF(AllenRelation.MEETS)>0
									|| a_ab.getF(AllenRelation.OVERLAPS)>0
									|| a_ab.getF(AllenRelation.CONTAINS)>0){
								//if yes, the edge a->b can be deleted.
								// (meaning, we add it to our list of deleted edges and
								// don't transfer it to the directly-follows-graph)
								deletedEdges.putIfAbsent(a.getLabel(),new HashSet<>());
								deletedEdges.get(a.getLabel()).add(b.getLabel());
								ab_deleted=true;
								++del_count;
								break;
							}
						}
					}
				}
				if(!ab_deleted)
					out.addDirectlyFollowsEdge(edge.getSource().getEvClass(),edge.getTarget().getEvClass(),fFollows);
			}


		}
		int dfcount=0,ccount=0;
		for(Long ignored : out.getDirectlyFollowsEdges()) {
			++dfcount;
		}
		for(Long ignored : out.getConcurrencyEdges()) {
			++ccount;
		}
		return new Object[]{out,String.format("  nodes:\t%d,\n" +
				"  input edges:\t%d,\n" +
				"  deleted edges:\t%d,\n" +
				"  df-edges:\t%d,\n" +
				"  ||-edges:\t%d,\n",
				out.getActivities().length,
				oldTNR.getEdges().size(),
				del_count,
				dfcount,
				ccount)};
	}
}
