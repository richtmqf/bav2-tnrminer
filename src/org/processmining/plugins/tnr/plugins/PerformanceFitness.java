/*
 * Copyright (c) 2017 Markus Richter.
 * This library is distributed under the terms of the Lesser General Public License, version 3.
 */

package org.processmining.plugins.tnr.plugins;

import org.apache.commons.collections15.MultiMap;
import org.apache.commons.collections15.multimap.MultiHashMap;
import org.deckfour.xes.classification.XEventClass;
import org.deckfour.xes.model.XLog;
import org.processmining.contexts.uitopia.annotations.UITopiaVariant;
import org.processmining.framework.plugin.PluginContext;
import org.processmining.framework.plugin.annotations.Plugin;
import org.processmining.plugins.tnr.common.*;
import org.processmining.processtree.Block;
import org.processmining.processtree.Node;
import org.processmining.processtree.Task;

import java.util.*;
import java.util.stream.Collectors;

public class PerformanceFitness {
	private static final int SAMPLE_SIZE=200;
	private static final int K=1000;



	@Plugin(
			name = "Measure Performance Fitness of an EnrichedTree",
			parameterLabels = {"Tree", "Log to compare with"},
			returnLabels = {"Measurements"},
			returnTypes = {String.class},
			help = "Measures the Performance Fitness as described in 'Temporal Network Representation of Event Logs for Improved Performance Modelling in Business Processes.' by Senderovich/Weidlich/Gal, 2017."
	)
	@UITopiaVariant(
			affiliation = "HU Berlin",
			author = "Markus Richter",
			email = "marku.online@googlemail.com"
	)
	static String measureFitness(PluginContext context, final EnrichedTree tree, XLog log){

		//select random samples
		Vector<IntervalTrace> copy = new Vector<>(new IntervalLog(log, TNRMiner.getEventClassifier(log)));
		Collections.shuffle(copy);
		int actualSampleSize = Math.min(SAMPLE_SIZE,copy.size());
		copy.setSize(actualSampleSize);
		double sum=0;

		StatisticAccumulator caseSojournTimeStatistics=new StatisticAccumulator();

		//measure results individually
		for(IntervalTrace trace : copy) {
			final long traceDuration = trace.getDuration();
			caseSojournTimeStatistics.addValue(traceDuration);
			long innerSqSum=0;
			for(int i = 0; i < K; i++) {
				final long replayDuration = measureTraceReplay(trace,tree);
				innerSqSum += (traceDuration-replayDuration)*(traceDuration-replayDuration);
			}
			sum += innerSqSum/K;
		}
		double pfs=sum/actualSampleSize;

		return "PFS= " + pfs + "  RMSD(PFS)= " + Math.sqrt(pfs) + "  Mean Case execution duration: " + caseSojournTimeStatistics.getMean() + "  %:" + 100*Math.sqrt(pfs)/caseSojournTimeStatistics.getMean();
	}

	/**
	 * measures the sojourn time of a replay of
	 * @param trace
	 * @param tree
	 * @return
	 */
	private static long measureTraceReplay(IntervalTrace trace, final EnrichedTree tree) {
		final HashSet<String> activities = trace.stream().map(IntervalEvent::getLbl).collect(Collectors.toCollection(HashSet::new));
		//first: for each or-node: decide which child-branch to use, according to the activities of the log (take the first which includes a necessary activity)
		Vector<Block> orNodes = tree.getNodes().stream()
				.filter(node -> (node instanceof Block.Xor || node instanceof Block.Or))
				.map(node -> (Block) node).collect(Collectors.toCollection(Vector::new));
		Map<Block, Node> branches = new HashMap<>();

		iteratingOverOrNodes:
		for(Block orNode : orNodes) {
			for(Node child : orNode.getChildren()) {
				if(branchContainsActivity(child, activities)){
					branches.put(orNode,child);
					continue iteratingOverOrNodes;
				}
			}
			//no node found, choose an empty (tau) node
			for(Node child : orNode.getChildren()) {
				if(child instanceof Task.Automatic && !(child instanceof ConditionalDelay)) {
					branches.put(orNode, child);
					continue iteratingOverOrNodes;
				}
			}
			//else take the first node
			branches.put(orNode, orNode.getChildren().get(0));
		}

		//Step two: replay the tree.
		ReplayEnvironment env = new ReplayEnvironment(tree, activities, branches);
		env.addNext(tree.getRoot());
		return env.replay();
	}


	/**
	 * searches for a node which contains a leaf with one of the given activities
	 * @param node the node in which to search
	 * @param activities the activities for which is searched
	 * @return true, if one of the activities is found, else false.
	 */
	private static boolean branchContainsActivity(Node node, Set<String> activities){
			if(node instanceof Task.Manual && activities.contains(node.getName()))
				return true;
			else if(node instanceof Block) {
				for(Node child : ((Block) node).getChildren()) {
					if(branchContainsActivity(child,activities))
						return true;
				}
			}
		return false;
	}


}

class CondDelayEvent{
	ConditionalDelay delayNode;
	Set<EventClass> openDependencies;

	public CondDelayEvent(ConditionalDelay delayNode) {
		this.delayNode = delayNode;
		this.openDependencies = delayNode.cloneDependencies();
	}

	public void removeDependency(ReplayEnvironment env,EventClass activity){
		openDependencies.remove(activity);
		if(dependenciesSatisfied()){
			env.addNext(delayNode.getTarget());
		}
	}

	public boolean dependenciesSatisfied() {
		return openDependencies.size()==0;
	}
}

/**
 * An environment which contains all the State variables for replaying a trace of a log in a given tree.
 * The important components are the tree and the priorityQueue nextEvents, which is sorted by the ending time of the events in ascending order.
 * The Main loop ({@link this.replay}) gets the event which will end next and will traverse the tree upwards to
 *   determine the nodes which could be started now, samples the duration and sets the current time as start time.
 * All Events for which the start time and the duration have been set will be inserted into the queue and can be considered 'started'.
 *
 *
 * The algorithm to traverse the Tree can be considered similar to a sweepline-algorithm.
 */
class ReplayEnvironment{

	private PriorityQueue<IntervalEvent> nextEvents = new PriorityQueue<>(Comparator.comparing(IntervalEvent::getCompleteTimestamp));
	private final EnrichedTree ctree;

	private Set<String> allowedActivities;
	private Set<EventClass> completedActivities = new HashSet<>();

	private long currentTime =0;
	private Map<Block, Node> orBranches;
	private Map<IntervalEvent,Task.Manual> ev2node = new HashMap<>();

	private Map<Block.And,Integer> completeAndCounter = new HashMap<>();

	private MultiMap<XEventClass, CondDelayEvent> dependants = new MultiHashMap<>();


	public ReplayEnvironment(EnrichedTree ctree, Set<String> allowedActivities, Map<Block, Node> orBranches) {
		this.ctree = ctree;
		this.allowedActivities = allowedActivities;
		this.orBranches = orBranches;
	}


	/**
	 * Add all currently executable children of treeNode as events to the PriorityQueue.
	 * @param treeNode
	 */
	void addNext(Node treeNode){
		if(treeNode instanceof Task.Manual) {
			EventClass activity = ctree.getActivity((Task.Manual) treeNode);
			if(!allowedActivities.contains(treeNode.getName())) {
				//node is not in the target log, so it's ignored (treated the same as an empty transition)
				handleDoneActivity(((Task.Manual) treeNode));
			} else {
				//node is in the target log: sample the duration, then create an event given the current time and
				// the duration and add it to the queue. Also remember to which node the event belongs.
				IntervalEvent ev = new IntervalEvent(treeNode.getName(), currentTime, activity.sampleDuration());
				nextEvents.add(ev);
				ev2node.put(ev, ((Task.Manual) treeNode));
			}
		} else if(treeNode instanceof ConditionalDelay){
			//reduce open dependencies to allowedEvents and not completed Events
			CondDelayEvent cde = new CondDelayEvent(((ConditionalDelay) treeNode));
			cde.openDependencies.removeIf(e -> !allowedActivities.contains(e.getId()));
			cde.openDependencies.removeAll(completedActivities);

			//if the dependencies are already satisfied, add the node after the DelayNode.
			if(cde.dependenciesSatisfied()){
				addNext(cde.delayNode.getTarget());
			}else{
				//if not, add the dependencyNode to each entry of a dependency. Now dep can notify cde if it's complete.
				for(EventClass dep : cde.openDependencies) {
					dependants.put(dep, cde);
				}
			}
		} else if(treeNode instanceof Task.Automatic){
			//a tau node is completed instantly.
			handleDoneActivity((Task) treeNode);
		} else if(treeNode instanceof Block.Xor || treeNode instanceof Block.Or){
			//in case of xor delegate to the previously destined child-node.
			addNext(orBranches.get(treeNode));
		} else if(treeNode instanceof Block.Seq){
			//in case of seq delegate to the first node.
			addNext(((Block.Seq) treeNode).getChildren().get(0));
		} else if(treeNode instanceof Block.And){
			List<Node> children = ((Block.And) treeNode).getChildren();
			completeAndCounter.put(((Block.And) treeNode), children.size());
			for(Node childNode : children) {
				addNext(childNode);
			}
		}

	}

	/**
	 * handle the completion of an activity which is currently(at currentTime) completed by traversing the Process tree
	 * upwards and searching for a new branch whose activities can now be added to the queue.
	 * @param treeNode
	 */
	private void handleDoneActivity(Task treeNode){
		EventClass activity=null;
		//get the assigned activity of the node (null if node is a silent node)
		if(treeNode!=null && treeNode instanceof Task.Manual)
			activity = ctree.getActivity((Task.Manual) treeNode);
		//if it's not a silent node, note it down as completed and
		// remove the node from all conditional delays which depend on it.
		if(activity!=null){
			completedActivities.add(activity);
			if(dependants.get(activity)!=null){
				for(CondDelayEvent condDelayEvent : dependants.get(activity)) {
					condDelayEvent.removeDependency(this, activity);
				}
				dependants.remove(activity);
			}
		}

		Node currentNode=treeNode, lastNode;
		//go upwards the tree and find next node to add, if possible.
		//decrement when reaching an and node and if reaching 0, pass through.
		//when reaching a seq node, add next element.
		while(!currentNode.isRoot()){
			lastNode=currentNode;
			currentNode=currentNode.getParents().iterator().next();
			if(currentNode instanceof Block.And){
				int a=completeAndCounter.get(currentNode)-1;
				completeAndCounter.put((Block.And) currentNode, a);
				if(a>0)
					break;
			}else if(currentNode instanceof Block.Seq){
				//find position of child
				boolean added_value=false;
				List<Node> children = ((Block.Seq) currentNode).getChildren();
				for(int i = 0; i < children.size(); i++) {
					Node node = children.get(i);
					//if there is a next node in the seq block, add it to the queue
					if(node == lastNode && i + 1 < children.size()) {
						addNext(children.get(i + 1));
						added_value = true;
					}
				}
				//if we added a node, the seq is not completed, therefore stop traversing the tree upwards.
				if(added_value)
					break;
			}
		}


	}

	/**
	 * Main Loop. gets the event which is ending next and sets the current time to its completion time.
	 *
	 * @return the last completion time, which also corresponds to the whole execution time.
	 */
	long replay(){
		while(nextEvents.peek()!=null){
			IntervalEvent ev = nextEvents.poll();
			currentTime =ev.getCompleteTimestamp().getTime();
			handleDoneActivity(ev2node.get(ev));
		}
		return currentTime;
	}
}

