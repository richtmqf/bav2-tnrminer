/*
 * Copyright (c) 2017 Markus Richter.
 * This library is distributed under the terms of the Lesser General Public License, version 3.
 */

package org.processmining.plugins.tnr.plugins;

import org.apache.commons.collections15.MultiMap;
import org.apache.commons.collections15.multimap.MultiHashMap;
import org.deckfour.xes.model.XLog;
import org.processmining.contexts.uitopia.annotations.UITopiaVariant;
import org.processmining.framework.plugin.PluginContext;
import org.processmining.framework.plugin.annotations.Plugin;
import org.processmining.plugins.InductiveMiner.mining.MiningParameters;
import org.processmining.plugins.InductiveMiner.mining.MiningParametersIMlc;
import org.processmining.plugins.InductiveMiner.plugins.IMProcessTree;
import org.processmining.plugins.tnr.common.*;
import org.processmining.processtree.Block;
import org.processmining.processtree.Node;
import org.processmining.processtree.ProcessTree;
import org.processmining.processtree.Task.Manual;
import org.processmining.processtree.impl.AbstractBlock;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

public class EnrichProcessTree {

	@Plugin(
			name = "Annotate ProcessTree with time-values and create Conditional Delays.",
			parameterLabels = {"TNR", "Process Tree"},
			returnLabels = {"EnrichedTree"},
			returnTypes = {EnrichedTree.class},
			help = "Generates a time-annotated Process Tree."
	)
	@UITopiaVariant(
			affiliation = "HU Berlin",
			author = "Markus Richter",
			email = "marku.online@googlemail.com"
	)
	public static EnrichedTree dependencyTree(PluginContext context, TNRGraph tnr, ProcessTree inputtree) {
		DependencyMapping dependencies = new DependencyMapping();
		return dependencyTree(context, dependencies, tnr, inputtree);
	}

	@Plugin(
			name = "Derive EnrichedTree with dependencies",
			parameterLabels = {/*"Dependencies", "TNR",*/ "Process Log"},
			returnLabels = {"EnrichedTree with Dependencies"},
			returnTypes = {EnrichedTree.class},
			//returnTypes = {DepGraph.class, String.class},
			help = "Generates a time-annotated Process Tree with Conditional Delays from a Log."
	)
	@UITopiaVariant(
			affiliation = "HU Berlin",
			author = "Markus Richter",
			email = "marku.online@googlemail.com"
	)
	public static EnrichedTree logTree1(PluginContext context, XLog log) {
		return treeCommon(context, log,true);
	}

	@Plugin(
			name = "Derive EnrichedTree without dependencies",
			parameterLabels = {/*"Dependencies", "TNR",*/ "Process Log"},
			returnLabels = {"EnrichedTree"},
			returnTypes = {EnrichedTree.class},
			help = "Generates a time-annotated Process Tree from a Log."
	)
	@UITopiaVariant(
			affiliation = "HU Berlin",
			author = "Markus Richter",
			email = "marku.online@googlemail.com"
	)
	public static EnrichedTree logTree2(PluginContext context, XLog log) {
		return treeCommon(context, log,false);
	}


	static EnrichedTree treeCommon(PluginContext context, XLog log, boolean withDeps){
		TNRGraph tnr = TNRMiner.mineTNR(context, log);

		//get tree
		MiningParameters param = new MiningParametersIMlc();
		param.setClassifier(TNRMiner.getEventClassifier(log));
		//circumvent UI and call IMlc directly.
		ProcessTree inputtree = IMProcessTree.mineProcessTree(log, param, () -> context.getProgress().isCancelled());
		//get dependencyMapping, if neccessary
		DependencyMapping dependencies = withDeps ?
				(DependencyMapping) DependencyDelayFinder.discoverDependencies(context, log, tnr)[0]
				:new DependencyMapping();
		return dependencyTree(context, dependencies, tnr, inputtree);
	}


	@Plugin(
			name = "Annotate ProcessTree with time-values and create Conditional Delays.",
			parameterLabels = {"Dependencies", "TNR", "Process Tree"},
			returnLabels = {"EnrichedTree with conditional delays"},
			returnTypes = {EnrichedTree.class},
			help = "Generates a time-annotated Process Tree with Conditional Delays."
	)
	@UITopiaVariant(
			affiliation = "HU Berlin",
			author = "Markus Richter",
			email = "marku.online@googlemail.com"
	)
	public static EnrichedTree dependencyTree(PluginContext context, DependencyMapping dependencies, TNRGraph tnr, ProcessTree inputtree) {

		//clone tree
		EnrichedTree tree = new EnrichedTree(inputtree);
		//first step: map TNRNodes to tree nodes and vice-versa
		MultiMap<EventClass, Manual> tnr2tree = new MultiHashMap<>();
		Map<Manual, EventClass> tree2tnr = new HashMap<>();
		for(TNRNode tnrNode : tnr.getNodes()) {
			for(Node treeNode : tree.getNodes()) {
				if(treeNode.isLeaf() && treeNode instanceof Manual && tnrNode.getEvClass().getId().equals(treeNode.getName())) {
					tree2tnr.put(((Manual) treeNode), tnrNode.getEvClass());
					tnr2tree.put(tnrNode.getEvClass(), ((Manual) treeNode));
					System.err.printf("  %s\t|%s\t %d %s\n", tnrNode, treeNode, treeNode.numParents(), ((Manual) treeNode).getOriginators());
				}
			}
		}
		tree.setActivityMapping(tree2tnr);

		//second step: add ConditionalDelay nodes
		for(EventClass activity : dependencies.keySet()) {
			for(Manual treenode : tnr2tree.get(activity)) {
				//there can only be one parent!
				Block parentNode = treenode.getParents().iterator().next();

				//get index of node in parent
				int index = parentNode.getChildren().size();
				List<Node> children = parentNode.getChildren();
				for(int i = 0; i < children.size(); i++) {
					if(children.get(i).getID().equals(treenode.getID())) {
						index = i;
						break;
					}
				}

				ConditionalDelay delay = new ConditionalDelay(dependencies.get(activity), treenode);
				delay.setProcessTree(tree);
				tree.addNode(delay);
				//add new conditional delay before activity
				if(parentNode instanceof Block.Seq) {
					//if activity is in a seq block, add delay directly before the activity
					parentNode.addChildAt(delay, index);
				} else {
					//if it is in another block, combine to a sequence block in place of the old activity.
					Block.Seq delayblock = new AbstractBlock.Seq("");
					delayblock.setProcessTree(tree);
					tree.addNode(delayblock);

					parentNode.swapChildAt(delayblock, index);
					delayblock.addChild(delay);
					delayblock.addChild(treenode);
				}
			}
		}

		return tree;
	}

}
