/*
 * Copyright (c) 2017 Markus Richter.
 * This library is distributed under the terms of the Lesser General Public License, version 3.
 */

package org.processmining.plugins.tnr.plugins;

import org.deckfour.xes.classification.XEventClassifier;
import org.deckfour.xes.extension.std.XLifecycleExtension;
import org.deckfour.xes.model.XEvent;
import org.deckfour.xes.model.XLog;
import org.deckfour.xes.model.XTrace;
import org.processmining.contexts.uitopia.annotations.UITopiaVariant;
import org.processmining.framework.plugin.PluginContext;
import org.processmining.framework.plugin.annotations.Plugin;

import java.util.HashMap;
import java.util.Map;

public class FilterLoops {
	@Plugin(
			name = "filter Log for Loops",
			parameterLabels = {"input log"},
			returnLabels = {"filtered log"},
			returnTypes = {XLog.class},//TNRGraph.class},
			help = "Returns the input log without traces that contain the same activity more than once."
	)
	@UITopiaVariant(
			affiliation = "HU Berlin",
			author = "Markus Richter",
			email = "marku.online@googlemail.com"
	)
	public static XLog filterLoops(PluginContext context, final XLog log){
		//some statistics
		//traces removed because of unexpected transitions:
		int rmUnexpected=0;
		//traces removed because of double activity+transition:
		int rmDouble=0;
		//traces removed because of missing matching transition:
		int rmTransition=0;

		XLog out= (XLog) log.clone();
		out.clear();
		XEventClassifier classifier=TNRMiner.getEventClassifier(log);
		logiteration:
		for(XTrace trace : log) {
			//for each activity,record if it has a start- and a complete-event
			Map<String, boolean[]> activityList = new HashMap<>();
			for(XEvent event : trace) {
				String activity = classifier.getClassIdentity(event);
				activityList.putIfAbsent(activity, new boolean[]{false, false});

				XLifecycleExtension.StandardModel transition = XLifecycleExtension.instance().extractStandardTransition(event);
				int transitionIndex;
				if(transition == XLifecycleExtension.StandardModel.START){
					transitionIndex=0;
				}else if(transition == XLifecycleExtension.StandardModel.COMPLETE){
					transitionIndex=1;
				}else{
					//an event with an unexpected transition type occurred, ignore trace.
					rmUnexpected++;
					continue logiteration;
				}
				// an event where this activity and transition already appeared occured, ignore trace.
				if(activityList.get(activity)[transitionIndex]) {
					rmDouble++;
					continue logiteration;
				}
				activityList.get(activity)[transitionIndex]=true;
			}
			//check if all events have a start and complete event. if there is one which hasn't, ignore this trace.
			for(String s : activityList.keySet()) {
				boolean[] val=activityList.get(s);
				if(!(val[0] && val[1])) {
					rmTransition++;
					// disable for now, but keep for statistics
// 					continue logiteration;
					break;
				}
			}
			//if no check was false, add the trace to the output log.
			out.add((XTrace) trace.clone());
		}
		System.err.printf("FilterLoops: Traces removed: %d + %d + %d = %d / %d",
				rmUnexpected,rmDouble,rmTransition, rmUnexpected+rmDouble+rmTransition,log.size());
		return out;
	}
}
