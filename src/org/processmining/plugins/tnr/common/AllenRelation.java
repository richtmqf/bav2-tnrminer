/*
 * Copyright (c) 2017 Markus Richter.
 * This library is distributed under the terms of the Lesser General Public License, version 3.
 */

package org.processmining.plugins.tnr.common;

/**
 * Defines Allen's interval relations as described in his paper[1].
 *
 * @author Markus Richter
 * [1] James F Allen. Maintaining knowledge about temporal intervals. Communications
 * of the ACM, 26(11):832–843, 1983.
 */
public enum AllenRelation {

	PRECEDES(0,"precedes"),
	MEETS(1,"meets"),
	OVERLAPS(2,"overlaps"),
	IS_FINISHED_BY(3,"is finished by"),
	CONTAINS(4,"contains"),
	STARTS(5,"starts"),
	EQUALS(6,"equals"),
	IS_STARTED_BY(7,"is started by"),
	DURING(8,"during"),
	FINISHES(9,"finished"),
	IS_OVERLAPPED_BY(10,"is overlapped by"),
	IS_MET_BY(11,"is met by"),
	IS_PRECEDED_BY(12,"is preceded by");

	private static AllenRelation[] val2rel = new AllenRelation[]{ PRECEDES, MEETS, OVERLAPS, IS_FINISHED_BY, CONTAINS, STARTS, EQUALS, IS_STARTED_BY, DURING, FINISHES, IS_OVERLAPPED_BY, IS_MET_BY, IS_PRECEDED_BY};

	private String txt;
	private int i;
	AllenRelation(int i,String txt) {
		this.i = i;
		this.txt = txt;

	}

	/**
	 * Determines the fitting relation for the given interval start and completion points
	 *
	 * @param start1 Start of the first interval
	 * @param end1   End of the first interval
	 * @param start2 Start of the second interval
	 * @param end2   End of the second interval
	 * @return the Interval Relation identifier as described in the paper.
	 */
	public static AllenRelation determine(long start1, long end1, long start2, long end2) {
		if(start1==end1){
			if(start2==end2)
				return determineInstants(start1,start2);
			else
				return determineInstantA(start1, start2, end2);
		} else
			if(start2==end2)
				return determineInstantB(start1, end1, start2);
			//else:
		if(start1 < start2) {
			if(end1 < start2) return PRECEDES;
			if(end1 == start2) return AllenRelation.MEETS;
			//else ( end1 > start2) :
			if(end1 < end2) return AllenRelation.OVERLAPS;
			if(end1 == end2) return AllenRelation.IS_FINISHED_BY;
			if(end1 > end2) return AllenRelation.CONTAINS;
			throw new IllegalArgumentException("Arguments are not describing non-empty intervals!");
		}
		if(start1 == start2) {
			if(end1 < end2) return AllenRelation.STARTS;
			if(end1 == end2) return AllenRelation.EQUALS;
			if(end2 < end1) return AllenRelation.IS_STARTED_BY;
			throw new RuntimeException("I really don't know how you got here!");
		}
		// else ( start1 > start2){
		if(end2 < start1) return AllenRelation.IS_PRECEDED_BY;
		if(end2 == start1) return AllenRelation.IS_MET_BY;
		// if(end2 > start1)
		if(end2 < end1) return AllenRelation.IS_OVERLAPPED_BY;
		if(end1 == end2) return AllenRelation.FINISHES;
		if(end1 < end2) return AllenRelation.DURING;
		throw new IllegalArgumentException("Arguments are not describing non-empty intervals!");

	}
	/**
	 * Determines the fitting relation for the given interval start and completion points
	 * (NOT EXPLICITLY DEFINED BY ALLEN)
	 *
	 * @param x1 Start and End of the first interval
	 * @param start2 Start of the second interval
	 * @param end2   End of the second interval
	 * @return the Interval Relation identifier as described in the paper.
	 */
	private static AllenRelation determineInstantA(long x1, long start2, long end2) {
		if(x1<start2)
			return PRECEDES;
		if(x1==start2)
			return MEETS;//could also be overlaps or starts
		if(x1>start2 && x1<end2)
			return DURING;
		if(x1==end2)
			return IS_MET_BY;//could also be is_overlapped_by or finishes
		if(x1>end2)
			return IS_PRECEDED_BY;
		//doesn't happen!
		throw new IllegalArgumentException("Arguments are not describing non-negative intervals!");
	}



	/**
	 * Determines the fitting relation for the given interval start and completion points
	 * (NOT EXPLICITLY DEFINED BY ALLEN)
	 *
	 * @param start1 Start of the first interval
	 * @param end1   End of the first interval
	 * @param x2   Start and End of the second interval
	 * @return the Interval Relation identifier as described in the paper.
	 */
	private static AllenRelation determineInstantB(long start1, long end1, long x2) {
		if(start1 > x2)
			return IS_PRECEDED_BY;
		if(start1 == x2)
			return IS_MET_BY;//could also be is_overlapped_by or finishes
		if(start1 < x2 && end1 > x2)
			return CONTAINS;
		if(end1 == x2)
			return MEETS;//could also be overlaps or starts
		if(end1 < x2)
			return PRECEDES;
		//doesn't happen!
		throw new IllegalArgumentException("Arguments are not describing non-negative intervals!");
	}

	/**
	 * Determines the fitting relation for the given interval start and completion points
	 * (NOT EXPLICITLY DEFINED BY ALLEN)
	 *
	 * @param x1   Start and End of the first interval
	 * @param x2   Start and End of the second interval
	 * @return the Interval Relation identifier as described in the paper.
	 */
	private static AllenRelation determineInstants(long x1, long x2) {
		if(x1<x2)
			return PRECEDES;
		if(x1==x2)
			return EQUALS;
		if(x1 > x2)
			return IS_PRECEDED_BY;
		//doesn't happen!
		throw new IllegalArgumentException("Arguments are not describing non-negative intervals!");
	}
	/**
	 * Determines the fitting relation for the given interval start and completion points, but the order
	 * of the given intervals is not respected, so the intervals will be treated as they were first ordered
	 * by their starting and end points, in that order.
	 * <p>
	 * For example an Interval relationship which would result in the Relation identifier {@link #IS_PRECEDED_BY}
	 * , would be returned as {@link #PRECEDES}.
	 *
	 * @param start1 Start of the first interval
	 * @param end1   End of the first interval
	 * @param start2 Start of the second interval
	 * @param end2   End of the second interval
	 * @return the Interval Relation identifier as described in the paper.
	 * @see #determine(long, long, long, long)
	 */
	static AllenRelation determineBidirectional(long start1, long end1, long start2, long end2) {
		AllenRelation out = determine(start1, end1, start2, end2);
		switch(out) {
			case DURING:
				return AllenRelation.CONTAINS;
			case FINISHES:
				return AllenRelation.IS_FINISHED_BY;
			case IS_OVERLAPPED_BY:
				return AllenRelation.OVERLAPS;
			case IS_MET_BY:
				return AllenRelation.MEETS;
			case IS_PRECEDED_BY:
				return AllenRelation.PRECEDES;
			case IS_STARTED_BY:
				return AllenRelation.STARTS;
			default:
				return out;
		}
	}
	static AllenRelation valueOf(int i){
		if(i<13 && i>=0)
			return val2rel[i];
		else
			throw new IllegalArgumentException("i must be an integer between 0 and 12 (including both values)");

	}

	/**
	 * Returns a plaintext which describes the relation.
	 *
	 * @return e.g. {@code "precedes"}
	 */
	@Override
	public String toString() {
		return txt;
	}

	/**
	 * Returns an integer which can identify the relation.
	 *
	 * @return e.g. {@code "precedes"}
	 */
	public int toInt() {
		return i;
	}

	public String toString(String a, String b) {
		return a + " " + txt + " " + b;
	}
}
