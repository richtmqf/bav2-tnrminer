/*
 * Copyright (c) 2017 Markus Richter.
 * This library is distributed under the terms of the Lesser General Public License, version 3.
 */

package org.processmining.plugins.tnr.common;

import org.deckfour.xes.classification.XEventClassifier;
import org.deckfour.xes.model.XLog;
import org.deckfour.xes.model.XTrace;

import java.util.HashSet;

/**
 * A Simple Log representation for easier usability.
 * @author Markus Richter
 */
public class IntervalLog extends HashSet<IntervalTrace> {
	/**
	 * Constructs an IntervalLog from an XLog and a Classifier.
	 * @param log
	 * @param xec
	 */
	public IntervalLog(XLog log, XEventClassifier xec){
		for(XTrace trace : log) {
			this.add(new IntervalTrace(trace,xec));
		}
	}
}

