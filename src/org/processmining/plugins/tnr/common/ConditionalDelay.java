/*
 * Copyright (c) 2017 Markus Richter.
 * This library is distributed under the terms of the Lesser General Public License, version 3.
 */

package org.processmining.plugins.tnr.common;

import org.processmining.processtree.Task;
import org.processmining.processtree.impl.AbstractTask;

import java.util.Collection;
import java.util.HashSet;
import java.util.Set;

/**
 * implements ConditionalDelay nodes for Process tree by deriving the silent-node-class Automatic.
 * Each ConditionalDelay has dependencies and also remembers which node can be started if those are satisfied.
 */
public class ConditionalDelay extends AbstractTask.Automatic{

	private Set<EventClass> dependencies;
	private Task.Manual target;



	public ConditionalDelay(Collection<EventClass> dependencies, Task.Manual target) {
		super("cond");
		this.dependencies = new HashSet<>(dependencies);
		this.target = target;
	}

	public EnrichedTree getConditionalTree(){
		return (EnrichedTree) getProcessTree();
	}

	public Set<EventClass> cloneDependencies() {
		return new HashSet<>(dependencies);
	}


	public Task.Manual getTarget() {
		return target;
	}

}
