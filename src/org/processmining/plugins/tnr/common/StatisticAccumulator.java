/*
 * Copyright (c) 2017 Markus Richter.
 * This library is distributed under the terms of the Lesser General Public License, version 3.
 */

package org.processmining.plugins.tnr.common;

/**
 * A simple helper class to enable incremental Accumulation of statistics(mean and stddev)
 */
public class StatisticAccumulator {
	private double mean=0, variance_tmp =0;
	private long n=0;

	public synchronized void addValue(double x){
		//use welfords method, algorithm taken from http://jonisalonen.com/2013/deriving-welfords-method-for-computing-variance/
		++n;
		double mean_old=mean;
		mean = mean + (x-mean)/n;
		variance_tmp = variance_tmp + (x-mean)*(x-mean_old);
	}


	public synchronized double getMean(){
		return mean;
	}

	public synchronized double getStdDev(){
		return Math.sqrt(variance_tmp/(n-1));
	}

	public synchronized long getCount(){
		return n;
	}
}
