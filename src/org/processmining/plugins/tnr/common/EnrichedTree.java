/*
 * Copyright (c) 2017 Markus Richter.
 * This library is distributed under the terms of the Lesser General Public License, version 3.
 */

package org.processmining.plugins.tnr.common;

import org.processmining.processtree.ProcessTree;
import org.processmining.processtree.Task;
import org.processmining.processtree.impl.ProcessTreeImpl;

import java.util.HashMap;
import java.util.Map;

/**
 * An Annotated ProcessTree which contains also a mapping from nodes to activities, which themselves include their durations.
 */
public class EnrichedTree extends ProcessTreeImpl{
	private Map<Task.Manual,EventClass> node2activity;


	/**
	 * Clones another Tree and creates an empty mapping.
	 * @param tree
	 */
	public EnrichedTree(ProcessTree tree) {
		super(tree);
	}

	public void setActivityMapping(Map<Task.Manual,EventClass> mapping){
		node2activity = new HashMap<>(mapping);
	}

	public EventClass getActivity(Task.Manual n){
		if(n.getProcessTree() == this)
			return node2activity.get(n);
		else
			throw new IllegalArgumentException("Node " + n + " doesn't exist in this Tree!");
	}
}


