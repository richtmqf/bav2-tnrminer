/*
 * Copyright (c) 2017 Markus Richter.
 * This library is distributed under the terms of the Lesser General Public License, version 3.
 */

package org.processmining.plugins.tnr.common;


import org.processmining.models.graphbased.AttributeMap;
import org.processmining.models.graphbased.directed.DirectedGraph;
import org.processmining.models.graphbased.directed.DirectedGraphEdge;

import java.util.Arrays;
import java.util.HashSet;
import java.util.Set;

/**
 * This class describes an edge of a {@link TNRGraph}. It basically consists of four parts: the source and target node (activities),
 * the interval relations corresponding to temporal evidence between the two and the
 * frequency f (how often the relation was observed).
 *
 * @author Markus Richter
 */
public class TNREdge implements DirectedGraphEdge<TNRNode, TNRNode> {
	final private TNRNode source, target;
	//final private Map<AllenRelation, Integer> relations = new HashMap<>();
	private int[] rels = new int[7];
	final private TNRGraph parent;
	private AttributeMap attrMap = new AttributeMap();


	public TNREdge(TNRGraph parent, String fromLbl, String toLbl) {
		this.parent = parent;

		//get corresponding nodes or add them if needed.
		source = parent.getOrAddNode(fromLbl);
		target = parent.getOrAddNode(toLbl);
		attrMap.put("ProM_Vis_attr_edge end", AttributeMap.ArrowType.ARROWTYPE_TECHNICAL);


	}

	void incrementFrequency(AllenRelation relType, int incrementVal) {
		rels[relType.toInt()]+=incrementVal;
	}

	public void setFrequency(AllenRelation relType, int newF) {
		rels[relType.toInt()]=newF;
	}

	/**
	 * This function gets the Frequency of the Occurrence of the relation type in the log between the start node label and the end label.
	 * @param relType the relation type for which the frequency is requested.
	 *                Must be a relation where the first relation starts before or with the second activity.
	 * @return the observed frequency or 0 if no evidence for this relation type exists.
	 */
	public int getF(AllenRelation relType) {
		return rels[relType.toInt()];
	}

	@Override
	public TNRNode getSource() {
		return source;
	}

	@Override
	public TNRNode getTarget() {
		return target;
	}

	@Override
	public String getLabel() {
		StringBuilder sb = new StringBuilder();
		sb.append("{");
		for(int i = 0; i < rels.length; i++) {
			AllenRelation rel = AllenRelation.valueOf(i);
			int f = rels[i];
			if(f>0){
				sb.append("(");
				sb.append(rel.toString()).append(",");
				sb.append(f).append("), ");
			}
		}
		int l = sb.length();
		//if all frequencies are 0, return an empty String (no label)
		if(l==1)return "";
		//delete last comma and space
		sb.delete(l - 2, l);
		sb.append("}");

		return sb.toString();
	}

	@Override
	public DirectedGraph<?, ?> getGraph() {
		return parent;
	}

	@Override
	public AttributeMap getAttributeMap() {
		return attrMap;
	}


	@Override
	public int hashCode() {
		return parent.hashCode() ^ source.hashCode() ^ target.hashCode();

	}

	/**
	 * @return the Set of Relations that are occurring at least once in the log.
	 */
	public Set<AllenRelation> getRelations() {
		HashSet<AllenRelation> relations = new HashSet<>();
		for(int i = 0; i < rels.length; i++) {
			if(rels[i]>0){
				relations.add(AllenRelation.valueOf(i));
			}
		}
		return relations;
	}

	/**
	 * Check if the given edge and <code>this</code> edge are the same. Edges are the same,
	 * if they belong to the same Graph and have the same relation and connected nodes.
	 * The frequency can be different.
	 * <p>
	 * This is needed to find the corresponding edge in the graph and increment if necessary.
	 *
	 * @param o the given edge.
	 * @return true if the edges are the same.
	 */
	@Override
	public boolean equals(Object o) {
		if(o instanceof TNREdge) {
			TNREdge that = ((TNREdge) o);
			return this.getGraph() == that.getGraph()
					&& this.getSource() == that.getSource()
					&& this.getTarget() == that.getTarget();
		} else
			return false;
	}

	@Override
	public String toString() {
		return "TNREdge{" +
				"source=" + source +
				", target=" + target +
				", rels=" + Arrays.toString(rels) +
				'}';
	}
}
