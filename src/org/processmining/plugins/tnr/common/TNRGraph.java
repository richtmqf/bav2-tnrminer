/*
 * Copyright (c) 2017 Markus Richter.
 * This library is distributed under the terms of the Lesser General Public License, version 3.
 */

package org.processmining.plugins.tnr.common;

import org.processmining.models.graphbased.AttributeMap;
import org.processmining.models.graphbased.directed.DirectedGraph;
import org.processmining.models.graphbased.directed.DirectedGraphEdge;
import org.processmining.models.graphbased.directed.DirectedGraphNode;

import java.util.HashMap;
import java.util.HashSet;
import java.util.Map;
import java.util.Set;

public class TNRGraph implements DirectedGraph<TNRNode, TNREdge> {
	private Map<String, TNRNode> allNodes;
	//Cannot use a set (doesn't have Set.get()), so use a self identifying map
	private Map<TNREdge, TNREdge> allEdges;
	private Map<TNRNode, Set<TNREdge>> inEdges;
	private Map<TNRNode, Set<TNREdge>> outEdges;
	private AttributeMap attrMap = new AttributeMap();


	public TNRGraph() {
		allNodes = new HashMap<>();
		allEdges = new HashMap<>();
		inEdges = new HashMap<>();
		outEdges = new HashMap<>();
	}

	public TNRGraph(Set<TNRNode> nodes) {
		allNodes = new HashMap<>();
		allEdges = new HashMap<>();
		inEdges = new HashMap<>();
		outEdges = new HashMap<>();
		for(TNRNode node : nodes) {
			allNodes.put(node.getLabel(), node);
			inEdges.put(node, new HashSet<>());
			outEdges.put(node, new HashSet<>());
		}
	}

	/**
	 * Given a name of an activity, this function returns the corresponding node in the graph
	 * or creates a new one if necessary and adds it.
	 *
	 * @param label the name of the activity.
	 * @return the corresponding TNRNode within this graph.
	 */
	public TNRNode getOrAddNode(String label) {
		if(label == null) throw new NullPointerException("Label can't be null");
		//check if node already exists and return it.
		TNRNode node = allNodes.get(label);
		if(node != null) return node;

		//if not, create a new node and add it to the graph before returning it.
		node = new TNRNode(this, label);
		inEdges.put(node, new HashSet<>());
		outEdges.put(node, new HashSet<>());
		allNodes.put(label, node);
		return node;
	}

	/**
	 * adds an observed relation of two activities.
	 * The activities are added if needed
	 * and if a corresponding edge already exists, the cardinality of that edge will be incremented.
	 *
	 * @param fromLabel the name of the first activity
	 * @param toLabel   the name of the second activity
	 * @param rel       the interval relation between the two activities
	 */
	public void addOrIncrementEdge(String fromLabel, String toLabel, AllenRelation rel) {
		addOrIncrementEdge(fromLabel, toLabel, rel, 1);
	}

	/**
	 * adds an observed relation of two activities.
	 * The activities are added if needed
	 * and if a corresponding edge already exists, the cardinality of that edge will be incremented.
	 *
	 * @param fromLabel    the name of the first activity
	 * @param toLabel      the name of the second activity
	 * @param rel          the interval relation between the two activities
	 * @param incrementVal the frequency which is added to the existing frequency.
	 *
	 * @return the Edge which was modified.
	 */
	public TNREdge addOrIncrementEdge(String fromLabel, String toLabel, AllenRelation rel, int incrementVal) {
		if(fromLabel == null) throw new NullPointerException("fromLabel can't be null");
		if(toLabel == null) throw new NullPointerException("toLabel can't be null");
		if(rel == null) throw new NullPointerException("rel can't be null");
		//create or retrieve the edge between those two nodes.
		TNREdge e = getOrAddEdge(fromLabel, toLabel);
		//add the temporal evidence to the edge.
		e.incrementFrequency(rel, incrementVal);
		return e;
	}

	public TNREdge getOrAddEdge(String fromLabel, String toLabel) {
		if(fromLabel == null) throw new NullPointerException("fromLabel can't be null");
		if(toLabel == null) throw new NullPointerException("toLabel can't be null");
		//create an edge between those two nodes.
		TNREdge e = getEdgeOrNull(fromLabel, toLabel);
		//check if the edge already exists
		if(e == null) {
			e = new TNREdge(this, fromLabel, toLabel);
			//add the edge to the graph if it doesn't exist.
			allEdges.put(e, e);
			inEdges.get(e.getTarget()).add(e);
			outEdges.get(e.getSource()).add(e);
		}
		return e;
	}

	public TNREdge getEdgeOrNull(String fromLabel, String toLabel) {
		return allEdges.get(new TNREdge(this, fromLabel, toLabel));
	}

	@Override
	public Set<TNRNode> getNodes() {
		//as the nodes in inEdges and outEdges are synchronized, inEdges could also be replaced with outEdges here.
		return inEdges.keySet();
	}

	@Override
	public Set<TNREdge> getEdges() {
		return allEdges.keySet();
	}

	/**
	 * @param node
	 * @return all incoming edges of node
	 * @throws ClassCastException if node is not a TNRNode.
	 */
	@Override
	public Set<TNREdge> getInEdges(DirectedGraphNode node) {
		if(node instanceof TNRNode)
			return inEdges.get(node);
		else
			throw new ClassCastException();
	}

	/**
	 * @param node
	 * @return all outgoing edges of node
	 * @throws ClassCastException if node is not a TNRNode.
	 */
	@Override
	public Set<TNREdge> getOutEdges(DirectedGraphNode node) {
		if(node instanceof TNRNode)
			return outEdges.get(node);
		else
			throw new ClassCastException();
	}

	/**
	 * Removes an edge from the graph
	 *
	 * @param directedGraphEdge the edge to be removed
	 * @throws ClassCastException when the given edge is not a TNREdge.
	 */
	@Override
	public void removeEdge(DirectedGraphEdge directedGraphEdge) {
		if(directedGraphEdge == null) {
			new Throwable("WARNING: directedGraphEdge shouldn't be null.").printStackTrace(System.err);
		}
		if(directedGraphEdge instanceof TNREdge) {
			TNREdge edge = (TNREdge) directedGraphEdge;
			//remove from all edge structures.
			inEdges.get(edge.getTarget()).remove(edge);
			outEdges.get(edge.getSource()).remove(edge);
			allEdges.remove(edge);

		} else
			throw new ClassCastException();
	}

	/**
	 * Removes a node and all connected edges from the graph.
	 *
	 * @param directedGraphNode the node to be removed
	 * @throws ClassCastException when the given node os not a TNRNode.
	 */
	@Override
	public void removeNode(DirectedGraphNode directedGraphNode) {
		if(directedGraphNode == null) {
			new Throwable("WARNING: directedGraphNode shouldn't be null.").printStackTrace(System.err);
		}
		if(directedGraphNode instanceof TNRNode) {
			TNRNode node = (TNRNode) directedGraphNode;
			//remove all edges which have node as a target
			for(TNREdge e : inEdges.get(node)) {
				allEdges.remove(e);
			}
			inEdges.remove(node);
			//remove all edges which have node as a source
			for(TNREdge e : outEdges.get(node)) {
				allEdges.remove(e);
			}
			outEdges.remove(node);
			//remove the node itself
			allNodes.remove(node.getLabel());
		} else
			throw new ClassCastException();
	}


	@Override
	public int compareTo(DirectedGraph<TNRNode, TNREdge> that) {
		return 0;
	}

	@Override
	public String getLabel() {
		//MAYBE change label
		return "ThatsALabel";
	}

	@Override
	public TNRGraph clone() {
		TNRGraph tnew = new TNRGraph();
		//copy over to tnew
		for(TNREdge tnrEdge : this.getEdges()) {
			for(AllenRelation relation : tnrEdge.getRelations()) {
				tnew.addOrIncrementEdge(
						tnrEdge.getSource().getLabel(),
						tnrEdge.getTarget().getLabel(),
						relation,
						tnrEdge.getF(relation));
			}
		}
		return tnew;
	}

	@Override
	public DirectedGraph<?, ?> getGraph() {
		return this;
	}

	@Override
	public AttributeMap getAttributeMap() {
		return attrMap;
	}

}
