/*
 * Copyright (c) 2017 Markus Richter.
 * This library is distributed under the terms of the Lesser General Public License, version 3.
 */

package org.processmining.plugins.tnr.common;

import org.deckfour.xes.classification.XEventClassifier;
import org.deckfour.xes.extension.std.XConceptExtension;
import org.deckfour.xes.extension.std.XLifecycleExtension;
import org.deckfour.xes.extension.std.XTimeExtension;
import org.deckfour.xes.model.XEvent;
import org.deckfour.xes.model.impl.XEventImpl;

import java.util.Date;

/**
 * A Simple Overlay class for two XEvents which are describing the start and completion of the same event.
 *
 * @author Markus Richter
 */
public class IntervalEvent {

	private Date startTimestamp, completeTimestamp;
	private XEvent startEvent, completeEvent;
	private String lbl;
	private String eventInstance;
	private int addEventCount = 0;


	IntervalEvent(XEvent event, XEventClassifier xec){

		this.startTimestamp = XTimeExtension.instance().extractTimestamp(event);
		this.startEvent = event;
		this.completeEvent = event;
		this.completeTimestamp = XTimeExtension.instance().extractTimestamp(event);
		this.lbl = xec.getClassIdentity(event);
		this.eventInstance = XConceptExtension.instance().extractInstance(event);
	}

	void addXEvent(XEvent event) {
		if(XLifecycleExtension.instance().extractStandardTransition(event)==XLifecycleExtension.StandardModel.START) {
			this.startTimestamp = XTimeExtension.instance().extractTimestamp(event);
			this.startEvent = event;
		} else {
			this.completeTimestamp = XTimeExtension.instance().extractTimestamp(event);
			this.completeEvent = event;
		}
		if(++addEventCount>2) System.err.println("FAIL! " + addEventCount + " | " + lbl);
	}

	public Date getStartTimestamp() {
		return startTimestamp;
	}

	public Date getCompleteTimestamp() {
		return completeTimestamp;
	}

	public XEvent getStartEvent() {
		return startEvent;
	}

	public XEvent getCompleteEvent() {
		return completeEvent;
	}

	public String getLbl() {
		return lbl;
	}

	public String getEventInstance() {
		return eventInstance;
	}


	public IntervalEvent(String lbl, long start, long duration) {
		this.lbl = lbl;
		startTimestamp = new Date(start);
		completeTimestamp = new Date(start+duration);

		startEvent = new XEventImpl();
		XLifecycleExtension.instance().assignStandardTransition(startEvent,XLifecycleExtension.StandardModel.START);
		XTimeExtension.instance().assignTimestamp(startEvent,startTimestamp);
		XConceptExtension.instance().assignName(startEvent,lbl);

		completeEvent = new XEventImpl();
		XLifecycleExtension.instance().assignStandardTransition(completeEvent,XLifecycleExtension.StandardModel.START);
		XTimeExtension.instance().assignTimestamp(completeEvent,completeTimestamp);
		XConceptExtension.instance().assignName(completeEvent,lbl);
	}
}
