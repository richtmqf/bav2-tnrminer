/*
 * Copyright (c) 2017 Markus Richter.
 * This library is distributed under the terms of the Lesser General Public License, version 3.
 */

package org.processmining.plugins.tnr.common;

import org.deckfour.xes.classification.XEventClassifier;
import org.deckfour.xes.extension.std.XConceptExtension;
import org.deckfour.xes.model.XEvent;
import org.deckfour.xes.model.XTrace;

import java.util.*;
import java.util.function.Consumer;
import java.util.function.Predicate;
import java.util.stream.Stream;
/**
 * A Trace-Implementation which focuses on IntervalEvents and on searchability by activity-name.
 * It also contains just one event for each transactional event and not two.
 * The Mapping is limited to one event per activity.
 * @author Markus Richter
 */
public class IntervalTrace implements Collection<IntervalEvent> {
	private HashMap<String, IntervalEvent> events = new HashMap<>();

	/**
	 * Constructs an IntervalTrace from an XTrace by mapping events to activities(which are derived by the Classifier.
	 * @param trace
	 * @param xec
	 */
	IntervalTrace(XTrace trace, XEventClassifier xec){
		for(XEvent event : trace) {

			String instanceID = getEventInstance(event);
			if(events.containsKey(instanceID)) {
				events.get(instanceID).addXEvent(event);
			} else {
				events.put(instanceID, new IntervalEvent(event, xec));
			}
		}
	}

	static String getEventInstance(XEvent event){
		String instanceID;
		if(event.getAttributes().containsKey(XConceptExtension.KEY_INSTANCE)){
			instanceID = XConceptExtension.instance().extractInstance(event);
		}else{
			instanceID = XConceptExtension.instance().extractName(event);
		}
		return instanceID;
	}

	public long getDuration(){
		long min = events.values().stream().min(Comparator.comparing(IntervalEvent::getStartTimestamp)).get().getStartTimestamp().getTime();
		long max = events.values().stream().max(Comparator.comparing(IntervalEvent::getCompleteTimestamp)).get().getCompleteTimestamp().getTime();
		return max-min;
	}

	@Override
	public int size() {
		return events.values().size();
	}

	@Override
	public boolean isEmpty() {
		return events.values().isEmpty();
	}

	@Override
	public boolean contains(Object o) {
		return events.values().contains(o);
	}

	@Override
	public Iterator<IntervalEvent> iterator() {
		return events.values().iterator();
	}

	@Override
	public Object[] toArray() {
		return events.values().toArray();
	}

	@Override
	public <T> T[] toArray(T[] a) {
		return events.values().toArray(a);
	}

	@Override
	public boolean add(IntervalEvent intervalEvent) {
		return events.values().add(intervalEvent);
	}

	@Override
	public boolean remove(Object o) {
		return events.values().remove(o);
	}

	@Override
	public boolean containsAll(Collection<?> c) {
		return events.values().containsAll(c);
	}

	@Override
	public boolean addAll(Collection<? extends IntervalEvent> c) {
		return events.values().addAll(c);
	}

	@Override
	public boolean removeAll(Collection<?> c) {
		return events.values().removeAll(c);
	}

	@Override
	public boolean removeIf(Predicate<? super IntervalEvent> filter) {
		return events.values().removeIf(filter);
	}

	@Override
	public boolean retainAll(Collection<?> c) {
		return events.values().retainAll(c);
	}

	@Override
	public void clear() {
		events.values().clear();
	}

	@Override
	public int hashCode() {
		return events.values().hashCode();
	}

	@Override
	public Spliterator<IntervalEvent> spliterator() {
		return events.values().spliterator();
	}

	@Override
	public Stream<IntervalEvent> stream() {
		return events.values().stream();
	}

	@Override
	public Stream<IntervalEvent> parallelStream() {
		return events.values().parallelStream();
	}

	@Override
	public void forEach(Consumer<? super IntervalEvent> action) {
		events.values().forEach(action);
	}
}
