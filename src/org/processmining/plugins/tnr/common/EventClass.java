/*
 * Copyright (c) 2017 Markus Richter.
 * This library is distributed under the terms of the Lesser General Public License, version 3.
 */

package org.processmining.plugins.tnr.common;

import org.deckfour.xes.classification.XEventClass;

import java.util.Random;

/**
 * A Class to describe an activity.
 * Additional to XEventClass, EventClass objects also contain the duration distribution information.
 */
public class EventClass extends XEventClass{
	private Random r = new Random();
	private StatisticAccumulator duration =new StatisticAccumulator();

	public EventClass(String id) {
		super(id, id.hashCode());
	}

	public void appendDuration(double x) {
		duration.addValue(x);
	}

	public double getDurationMean() {
		return duration.getMean();
	}

	public double getDurationStdDev() {
		return duration.getStdDev();
	}

	public long getEventCount() {
		return duration.getCount();
	}

	public long sampleDuration(){
		long val = (long) (r.nextGaussian()*duration.getStdDev()+duration.getMean());
		return val >= 0 ? val : sampleDuration();
//		return (long) duration.getMean();
	}
}
