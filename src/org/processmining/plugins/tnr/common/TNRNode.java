/*
 * Copyright (c) 2017 Markus Richter.
 * This library is distributed under the terms of the Lesser General Public License, version 3.
 */

package org.processmining.plugins.tnr.common;

import org.processmining.models.graphbased.AttributeMap;
import org.processmining.models.graphbased.NodeID;
import org.processmining.models.graphbased.directed.DirectedGraph;
import org.processmining.models.graphbased.directed.DirectedGraphNode;

/**
 * This class describes the node of a {@link TNRGraph}. It corresponds to an observed activity in an event log.
 * The label is the name of the activity and the other attributes are necessary
 * for the implementation of {@link DirectedGraphNode}.
 *
 * @author Markus Richter
 */
public class TNRNode implements DirectedGraphNode {
	private final EventClass evClass;

	private final TNRGraph parent;
	private final NodeID id = new NodeID();

	private AttributeMap attrMap = new AttributeMap();

	TNRNode(TNRGraph parent, String label) {
		this.parent = parent;
		this.evClass = new EventClass(label);
	}

	@Override
	public NodeID getId() {
		return id;
	}

	@Override
	public int compareTo(DirectedGraphNode that) {
		if(that instanceof TNRNode) {
			//TODO check if still applicable
			return this.getLabel().compareTo(that.getLabel());
		} else
			throw new ClassCastException();
	}

	@Override
	public String getLabel() {
		return evClass.getId();
	}

	public EventClass getEvClass() {
		return evClass;
	}

	@Override
	public DirectedGraph<?, ?> getGraph() {
		return parent;
	}

	@Override
	public AttributeMap getAttributeMap() {
		return attrMap;
	}

	@Override
	public int hashCode() {
		return id.hashCode();
	}

	@Override
	public boolean equals(Object obj) {
		if(obj instanceof TNRNode) {
			TNRNode that = ((TNRNode) obj);
			return this.getId().equals(that.getId());
		}
		return false;
	}

	@Override
	public String toString() {
		return "TNRn "+evClass;
//		return "TNRNode{" +
//				"evClass=" + evClass +
//				", id=" + id +
//				'}';
	}
}
