/*
 * Copyright (c) 2017 Markus Richter.
 * This library is distributed under the terms of the Lesser General Public License, version 3.
 */

package org.processmining.plugins.tnr.common;

import org.apache.commons.collections15.multimap.MultiHashMap;


/**
 * Just a alias class for the Dependency-Graph
 */
public class DependencyMapping extends MultiHashMap<EventClass,EventClass> {

}
