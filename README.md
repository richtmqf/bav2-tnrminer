# TNRDepends
Das ist der Implementationsteil der Bachelorarbeit von Markus Richter.

## Dependencies
- A java development environment (JDK) with version >=8
- Apache Ivy installed
- a Java-IDE that can execute/interpret .launch-configurations and works with ivy eg.: 
  - eclipse with IvyDE and Naturist if neccessary.
  - intellij with eclipser

## How to prepare the IDE
In Intellij, do:
0. download this project (via `git clone` or the import-project-wizard)
1. run `ivy -settings ivysettings.xml` in a commandline from the project directory, to load all dependencies.
2. In the project View, rightclick `ProM with UITopia (TNRMiner).launch` and select `Convert with Eclipser`. 
3. This creates a run configuration which you can then use to run or debug the source code within IntelliJ.

In Eclipse, do:
1. Download the project into your workspace
    1. Open `File`->`Import...` , select `Git/Projects from Git` and click `Next >`
    2. Select Clone URI, and follow the wizard until the next step. (master as the default branch is recommended.)
    3. In `Select a wizard to use for importing Projects` select `Import as general project` and click `Next >`.
    4. Set the project name to `TNRMiner` and click `Finish`.
2. Change the Project from general project to a Java project. One way is described below (needs the Naturist plugin from the marketplace):
    1. Right-click on the project directory and select `Properties...`.
    2. In the opened window select `Project Natures` and click `Add ...` and `Ok`.
    3. Select `Java` and click `Ok`. Then close the properties Window and wait until all operations are completed.
3. Right-click on the `ivy.xml`-File and select `Add Ivy Library...`. Click `Ok` on the Window and wait until a library-entry named Ivy appears.


# How to Start:
Select the fitting Run-Configuration (`ProM with UITopia (TNRMiner)` or similar) and press the run-Button.
